# Sniki's Wallpapers Collection

## Description
This is a repository with wallpapers that i have collected over a number of years from various wallpapers websites and renamed them with numbers instead so i can better manage them and also have them on a single place collected and have them whenever i need or after a clean install.
The vast majority of these wallpapers are nature, landscapes, abstract and technology related.

## Ownership
Because i downloaded most of these wallpapers from various websites, I have no way of knowing if there is a copyright on these images. If you find an image hosted in this repository that is yours and of limited use, please let me know and I will remove it.
